#include <stdio.h>
#include "prodRules.h"
#define P(produc_rule) #produc_rule //case rule: return #rule

char *getProdR(int P)
{
    switch (P)
    {
    case 5000:
        return "file";
        break;
    case 5001:
        return "package";
        break;
    case 5002:
        return "imports";
        break;
    case 5003:
        return "import";
        break;
    case 5004:
        return "import_stmt";
        break;
    case 5005:
        return "import_stmt_list";
        break;
    case 5006:
        return "import_here";
        break;
    case 5007:
        return "import_package";
        break;
    case 5008:
        return "import_safety";
        break;
    case 5009:
        return "import_there";
        break;
    case 5010:
        return "comon_dlc";
        break;
    case 5011:
        return "common_dcl";
        break;
    case 5012:
        return "_lconst";
        break;
    case 5013:
        return "vardcl";
        break;
    case 5014:
        return "constdcl";
        break;
    case 5015:
        return "constdcl1";
        break;
    case 5016:
        return "typedclname";
        break;
    case 5017:
        return "tydcl";
        break;
    case 5018:
        return "simple_stmt";
        break;
    case 5019:
        return "_case";
        break;
    case 5020:
        return "compound_stmt";
        break;
    case 5021:
        return "caseblock";
        break;
    case 5022:
        return "caseblock_list";
        break;
    case 5023:
        return "loop_body";
        break;
    case 5024:
        return "range_stmt";
        break;
    case 5025:
        return "for_header";
        break;
    case 5026:
        return "for_body";
        break;
    case 5027:
        return "for_stmt";
        break;
    case 5028:
        return "if_header";
        break;
    case 5029:
        return "if_stmt";
        break;
    case 5030:
        return "elseif";
        break;
    case 5031:
        return "elseif_list";
        break;
    case 5032:
        return "_else";
        break;
    case 5033:
        return "switch_stmt";
        break;
    case 5034:
        return "select_stmt";
        break;
    case 5035:
        return "expr";
        break;
    case 5036:
        return "uexpr";
        break;
    case 5037:
        return "pseudocall";
        break;
    case 5038:
        return "keyval";
        break;
    case 5039:
        return "bare_complitexpr";
        break;
    case 5040:
        return "complitsexpr";
        break;
    case 5041:
        return "pexpr";
        break;
    case 5042:
        return "expr_or_type";
        break;
    case 5043:
        return "new_name";
        break;
    case 5044:
        return "dcl_name";
        break;
    case 5045:
        return "onew_name";
        break;
    case 5046:
        return "sym";
        break;
    case 5047:
        return "hidden_importsym";
        break;
    case 5048:
        return "name";
        break;
    case 5049:
        return "labelname";
        break;
    case 5050:
        return "dotdotdot";
        break;
    case 5051:
        return "ntype";
        break;
    case 5052:
        return "non_expr_type";
        break;
    case 5053:
        return "non_recvchanttype";
        break;
    case 5054:
        return "convtype";
        break;
    case 5055:
        return "comptype";
        break;
    case 5056:
        return "fnret_type";
        break;
    case 5057:
        return "dotname";
        break;
    case 5058:
        return "othertype";
        break;
    case 5059:
        return "ptrtype";
        break;
    case 5060:
        return "recvchantype";
        break;
    case 5061:
        return "structtype";
        break;
    case 5062:
        return "interfacetype";
        break;
    case 5063:
        return "xfndcl";
        break;
    case 5064:
        return "fndcl";
        break;
    case 5065:
        return "hidden_fndcl";
        break;
    case 5066:
        return "fntype";
        break;
    case 5067:
        return "fnbody";
        break;
    case 5068:
        return "fnres";
        break;
    case 5069:
        return "fnlitdcl";
        break;
    case 5070:
        return "fnliteral";
        break;
    case 5071:
        return "xdcl_list";
        break;
    case 5072:
        return "vardcl_list";
        break;
    case 5073:
        return "constdcl_list";
        break;
    case 5074:
        return "typedcl_list";
        break;
    case 5075:
        return "structdcl_list";
        break;
    case 5076:
        return "interfacedcl_list";
        break;
    case 5077:
        return "structdcl";
        break;
    case 5078:
        return "packname";
        break;
    case 5079:
        return "embed";
        break;
    case 5080:
        return "interfacedcl";
        break;
    case 5081:
        return "indcl";
        break;
    case 5082:
        return "arg_type";
        break;
    case 5083:
        return "arg_type_list";
        break;
    case 5084:
        return "oarg_type_list_ocomma";
        break;
    case 5085:
        return "stmt";
        break;
    case 5086:
        return "non_dlc_stmt";
        break;
    case 5087:
        return "stmt_list";
        break;
    case 5088:
        return "new_name_list";
        break;
    case 5089:
        return "dcl_name_list";
        break;
    case 5090:
        return "expr_list";
        break;
    case 5091:
        return "expr_or_type_list";
        break;
    case 5092:
        return "keyval_list";
        break;
    case 5093:
        return "braced_keyval_list";
        break;
    case 5094:
        return "osemi";
        break;
    case 5095:
        return "ocomma";
        break;
    case 5096:
        return "oexpr";
        break;
    case 5097:
        return "oexpr_list";
        break;
    case 5098:
        return "osimple_stmt";
        break;
    case 5099:
        return "ohidden_funarg_list";
        break;
    case 5100:
        return "ohidden_structdcl_list";
        break;
    case 5101:
        return "ohidden_interfacedcl_list";
        break;
    case 5102:
        return "oliteral";
        break;
    case 5103:
        return "hidden_import";
        break;
    case 5104:
        return "hidden_pkg_importsym";
        break;
    case 5105:
        return "hidden_type";
        break;
    case 5106:
        return "hidden_type_non_recv_chan";
        break;
    case 5107:
        return "hidden_type_misc";
        break;
    case 5108:
        return "hidden_type_recv_chan";
        break;
    case 5109:
        return "hidden_type_func";
        break;
    case 5110:
        return "hidden_funarg";
        break;
    case 5111:
        return "hidden_structdlc";
        break;
    case 5112:
        return "hidden_interfacedlc";
        break;
    case 5113:
        return "ohidden_funres";
        break;
    case 5114:
        return "hidden_literal";
        break;
    case 5115:
        return "hidden_constant";
        break;
    case 5116:
        return "hidden_import_list";
        break;
    case 5117:
        return "hidden_funarg_list";
        break;
    case 5118:
        return "hidden_structdcl_list";
        break;
    case 5119:
        return "hidden_interfacedcl_list";
        break;
     case 5121:
        return "xdcl";
        break;
    case 5122:
        return "pexpr_no_paren";
        break;
    case 5123:
        return "lconst";
        break;
    case 5124:
        return "non_dcl_stmt_t";
        break;
    case 5125:
        return "non_recvchantype";
        break;
    case 5126:
        return "lbrace"; 
        break;
    case 5127:
        return "name_or_type"; 
        break;
    case 5128:
        return "complitexpr";
        break;
    case 5129: 
        return "else";
        break;
    case 5130:
        return "case";
        break;
    case 5131:
        return "typedcl";
        break;

    default:
        return "ERROR: Undefined Rule";
    }
}
