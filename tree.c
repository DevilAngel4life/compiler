
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include "lexe.h"
#include "tree.h"
#include "prodRules.h"

treeNode *treeA(int prodRule, int nkids, ...)
{
    va_list ap;
    treeNode *t = calloc(1, sizeof(struct tree) +
                                (nkids - 1) * sizeof(struct tree *));
    if (t == NULL)
    {
        fprintf(stderr, "Error: out of memory\n");
        exit(1);
    }
    t->leaf = NULL;
    t->prodrule = prodRule;
    t->nkids = nkids;

    if (nkids > 0 && nkids <= 9)
    {
        va_start(ap, nkids);
        for (int i = 0; i < nkids; i++)
        {
            t->kids[i] = va_arg(ap, treeNode *);
        }
        va_end(ap);
    }

    return t;
}

void printTree(treeNode *t, int depth)
{

    if (t == NULL)
    {
        printf("%*s Null\n", depth * 2, " ");
    }
    else if (t->nkids >= 0)
    {
        if ((t->leaf != NULL) && (t->leaf->text != NULL))
        {
            printf("%*s  %s : %i\n", depth * 2, " ", t->leaf->text,
                   t->nkids);
        }
        else
        {
            printf("%*s%s: %d\n", depth * 2, " ", getProdR(t->prodrule),
                   t->nkids);
        }

        for (int i = 0; i < t->nkids; i++)
        {
            printTree(t->kids[i], depth + 1);
        }
    }
    else
    {
        printf("Unknown error occurred inside printTree, exiting now.");
        exit(1);
    }
};

treeNode *getLeaf(struct token *lef)
{
    treeNode *t = malloc(sizeof(treeNode));
    t->prodrule = 0;
    t->nkids = 0;
    t->leaf = lef;
    return t;
}

void freeTree(treeNode *t)
{
    printf("hello");
}