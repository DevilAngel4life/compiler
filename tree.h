#ifndef TREE_H
#define TREE_H
#include <stdarg.h>
#include <stdlib.h>
#include "lexe.h"

typedef struct tree
{
    int prodrule;
    int nkids;
    struct tree *kids[9];
    token *leaf;
} treeNode;

treeNode *globalHead;
treeNode *nextTree[9];
int currentT;
treeNode *treeA(int prodRule, int nkids, ...);
treeNode *getLeaf(struct token *lef);
void printTree(treeNode *t, int depth);
int treeprint(treeNode *t, int depth);
void freeTree(treeNode *t);

#endif // TREE_H