// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file
// (a copy is at www2.cs.uidaho.edu/~jeffery/courses/445/go/LICENSE).

/*
 * Go language grammar adapted from Go 1.2.2.
 *
 * The Go semicolon rules are:
 *
 *  1. all statements and declarations are terminated by semicolons.
 *  2. semicolons can be omitted before a closing ) or }.
 *  3. semicolons are inserted by the lexer before a newline
 *      following a specific list of tokens.
 *
 * Rules #1 and #2 are accomplished by writing the lists as
 * semicolon-separated lists with an optional trailing semicolon.
 * Rule #3 is implemented in yylex.
 */

%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lexe.h"
#include "prodRules.h"
#include "tree.h"
int yylex();

void yyerror(char *s)
{
   fprintf(stderr, "%s:%d:  %s\n",
	   currFileName, yylineno, s);
	   exit(2);
}

#define YYDEBUG 1
	//   int yydebug = 1;
%}

// %define api.value.type { struct tree* }

%union {
	struct tree *t;
} 

%token 	<t>	LLITERAL LLITERALC LLITERALF LLITERALS LLITERALN
%token	<t>	LASOP LCOLAS
%token	<t>	LBREAK LCASE LCHAN LCONST LCONTINUE LDDD
%token	<t>	LDEFAULT LDEFER LELSE LFALL LFOR LFUNC LGO LGOTO
%token	<t>	LIF LIMPORT LINTERFACE LMAP LNAME
%token	<t>	LPACKAGE LRANGE LRETURN LSELECT LSTRUCT LSWITCH
%token	<t>	LTYPE LVAR

%token	<t>	LANDAND LANDNOT LCOMM LDEC LEQ LGE LGT
%token	<t>	LIGNORE LINC LLE LLSH LLT LNE LOROR LRSH

%type   <t> '@' '.' '?' ';' '(' ')' '=' ',' '[' ']' ':' '}' '{' '*' '~' '&' '+' '-' '!' 
%type  	<t> '|' '^'  '/' '%'	
%type 	<t> file imports  package import

%type 	<t>	lbrace import_here
%type 	<t>	sym packname
%type 	<t> import_stmt	import_stmt_list osemi import_package import_safety

%type 	<t> common_dcl xfndcl uexpr
%type	<t> vardcl vardcl_list lconst constdcl constdcl_list typedcl typedcl_list
%type	<t>	new_name dcl_name  typedclname
%type	<t>	oliteral

%type	<t>	stmt ntype	
%type	<t>	arg_type dotname fnlitdcl

%type	<t>	case caseblock
%type	<t>	compound_stmt  embed  complitexpr 
%type	<t>	expr_or_type expr bare_complitexpr
%type	<t>	fndcl fnliteral
%type	<t>	for_body for_header for_stmt if_header  non_dcl_stmt
%type	<t>	interfacedcl keyval labelname name
%type	<t>	name_or_type non_expr_type

%type	<t>	onew_name
%type	<t>	osimple_stmt pexpr pexpr_no_paren
%type	<t>	pseudocall range_stmt select_stmt
%type	<t>	simple_stmt	if_stmt switch_stmt
%type	<t>	  start_complit oexpr_list

%type	<t>	xdcl fnbody fnres loop_body dcl_name_list
%type	<t>	new_name_list expr_list keyval_list braced_keyval_list expr_or_type_list xdcl_list
%type 	<t> oarg_type_list_ocomma arg_type_list ocomma
%type	<t>	 caseblock_list elseif elseif_list else stmt_list  
%type	<t>	interfacedcl_list   structdcl structdcl_list
%type	<t>	constdcl1  
%type 	<t>	error

%type	<t>	convtype comptype dotdotdot	oexpr
%type	<t>	indcl interfacetype structtype ptrtype
%type	<t>	recvchantype non_recvchantype othertype fnret_type fntype

%type	<t>	hidden_importsym


%left		LCOMM	/* outside the usual hierarchy; here for good error messages */

%left		LOROR
%left		LANDAND
%left		LEQ LNE LLE LGE LLT LGT
%left		'+' '-' '|' '^'
%left		'*' '/' '%' '&' LLSH LRSH LANDNOT

/*
 * manual override of shift/reduce conflicts.
 * the general form is that we assign a precedence
 * to the token being shifted and then introduce
 * NotToken with lower precedence or PreferToToken with higher
 * and annotate the reducing rule accordingly.
 */
%left		NotPackage
%left		LPACKAGE

%left		NotParen
%left		'('

%left		')'
%left		PreferToRightParen

%%
file:	package	imports xdcl_list 		{$$ = treeA(FILE_T, 3, $1,$2,$3); nextTree[currentT] = $$;}
	;

package:
	%prec NotPackage 
	{
		yyerror("package statement must be first");
		exit(1);
	}
|	LPACKAGE sym ';'  					{ if(strcmp("main",$2->kids[0]->leaf->text)==0){
											$$ = treeA(PACKAGE_T, 3, $1,$2,$3);
											}
										else
											{
												yyerror("Error Package main required");	
											}
										}
	;


/*
*						Start of the import section
*	
*/
imports:	{$$=NULL;}
|	imports import ';'					{ $$ = treeA(IMPORTS_T, 3, $1,$2,$3);}
	;
import:
	LIMPORT import_stmt 					{ $$ = treeA(IMPORT_T, 2, $1,$2);}
|	LIMPORT '(' import_stmt_list osemi ')' 	{ yyerror("legal Go, not in VGo");}
|	LIMPORT '(' ')'							{ yyerror("legal Go, not in VGo");}
	;

import_stmt:
	import_here import_package			{ $$ = treeA(IMPORT_STMT_T, 2, $1,$2);}
|	import_here							{ $$ = treeA(IMPORT_STMT_T,1,$1);}
	;

import_stmt_list:
	import_stmt							{ $$ = treeA(IMPORT_STMT_LIST_T, 1,$1);}
|	import_stmt_list ';' import_stmt	{ $$ = treeA(IMPORT_STMT_LIST_T, 3, $1,$2,$3);}
	;

import_here:
	LLITERALS							{ $$ =  treeA(IMPORT_HERE_T,1, $1 );}
|	sym LLITERALS						{ $$ = treeA(IMPORT_HERE_T, 2, $1,$2);}
|	'.' LLITERALS						{ $$ = treeA(IMPORT_HERE_T, 2, $1,$2);}
	;

import_package: 						
	LPACKAGE LNAME import_safety ';'	{ $$ = treeA(IMPORT_PACKAGE_T, 4, $1,$2,$3,$4);}
	;

import_safety: 							{$$=NULL;}
|	LNAME								{ $$ =  treeA(IMPORT_SAFETY_T,1, $1 );}
	;

/*
 * 								declarations
 */

xdcl:
	{
		yyerror("empty top-level declaration");
		// $$ = 0;
	}
|	common_dcl							{ $$ = treeA(XDCL_T, 1, $1);}						
|	xfndcl								{ $$ = treeA(XDCL_T, 1, $1);}		
|	non_dcl_stmt						
	{
		yyerror("non-declaration statement outside function body");
		
	}
|	error
	{
		//$$ = 0;
		yyerror("non-declaration statement outside function body");
	}
	;

common_dcl:
	LVAR vardcl							{ $$ = treeA(COMMON_DCL_T, 2, $1,$2);}
|	LVAR '(' vardcl_list osemi ')'		{ $$ = treeA(COMMON_DCL_T, 5, $1,$2,$3,$4,$5);}
|	LVAR '(' ')'						{ $$ = treeA(COMMON_DCL_T, 3, $1,$2,$3);}
|	lconst constdcl						{ $$ = treeA(COMMON_DCL_T, 2, $1,$2);}
|	lconst '(' constdcl osemi ')'		{ $$ = treeA(COMMON_DCL_T, 5, $1,$2,$3,$4,$5);}
|	lconst '(' constdcl ';' constdcl_list osemi ')'	{ $$ = treeA(COMMON_DCL_T, 7, $1,$2,$3,$4,$5,$6,$7);}
|	lconst '(' ')'						{ $$ = treeA(COMMON_DCL_T, 3, $1,$2,$3);}
|	LTYPE typedcl						{ $$ = treeA(COMMON_DCL_T, 2, $1,$2);}
|	LTYPE '(' typedcl_list osemi ')'	{ $$ = treeA(COMMON_DCL_T, 5, $1,$2,$3,$4,$5);}
|	LTYPE '(' ')'						{ $$ = treeA(COMMON_DCL_T, 3, $1,$2,$3);}
	;

lconst:
	LCONST								{$$ = treeA(LCONST_T, 1,$1);}
	;

vardcl:
	dcl_name_list ntype					{ $$ = treeA(VARDCL_T, 2, $1,$2);}
|	dcl_name_list ntype '=' expr_list	{ $$ = treeA(VARDCL_T, 4, $1,$2,$3,$4);}
|	dcl_name_list '=' expr_list			{ $$ = treeA(VARDCL_T, 3, $1,$2,$3);}
	;

constdcl:
	dcl_name_list ntype '=' expr_list	{ $$ = treeA(CONSTDCL_T, 4, $1,$2,$3,$4);}
|	dcl_name_list '=' expr_list			{ $$ = treeA(CONSTDCL_T, 3, $1,$2,$3);}
	;

constdcl1:
	constdcl							{ $$ = treeA(CONSTDCL1_T, 1, $1);}					
|	dcl_name_list ntype					{ $$ = treeA(CONSTDCL1_T, 2, $1,$2);}
|	dcl_name_list						{ $$ = treeA(CONSTDCL1_T, 1, $1);}
	;

typedclname:
	sym
	{
		// the name becomes visible right here, not at the end
		// of the declaration.
	}
	;

typedcl:
	typedclname ntype					{ $$ = treeA(TYPEDCL_T, 2, $1,$2);}						
	;

simple_stmt:
	expr								{ $$ = treeA(SIMPLE_STMT_T, 1, $1);}	
|	expr LASOP expr						{ $$ = treeA(SIMPLE_STMT_T, 3, $1,$2,$3);}	
|	expr_list '=' expr_list				{ $$ = treeA(SIMPLE_STMT_T, 3, $1,$2,$3);}	
|	expr_list LCOLAS expr_list			{ $$ = treeA(SIMPLE_STMT_T, 3, $1,$2,$3);}	
|	expr LINC							{ $$ = treeA(SIMPLE_STMT_T, 2, $1,$2);}	
|	expr LDEC							{ $$ = treeA(SIMPLE_STMT_T, 2, $1,$2);}	
	;

case:
	LCASE expr_or_type_list ':'				{ $$ = treeA(CASE_T, 3, $1,$2,$3);}	
|	LCASE expr_or_type_list '=' expr ':'	{ $$ = treeA(CASE_T, 5, $1,$2,$3,$4,$5);}
|	LCASE expr_or_type_list LCOLAS expr ':'	{ $$ = treeA(CASE_T, 3, $1,$2,$3,$4,$5);}
|	LDEFAULT ':'							{ $$ = treeA(CASE_T, 2, $1,$2);}
	;

compound_stmt:
	'{'
	stmt_list '}'							{ $$ = treeA(COMPOUND_STMT_T, 3, $1,$2,$3);}	
	;

caseblock:
	case
	{
		// If the last token read by the lexer was consumed
		// as part of the case, clear it (parser has cleared yychar).
		// If the last token read by the lexer was the lookahead
		// leave it alone (parser has it cached in yychar).
		// This is so that the stmt_list action doesn't look at
		// the case tokens if the stmt_list is empty.
		// yylast = yychar;
	}
	stmt_list
	{
//		int last;

		// This is the only place in the language where a statement
		// list is not allowed to drop the final semicolon, because
		// it's the only place where a statement list is not followed 
		// by a closing brace.  Handle the error for pedantry.

		// Find the final token of the statement list.
		// yylast is lookahead; yyprev is last of stmt_list
		// last = yyprev;

		// if(last > 0 && last != ';' && yychar != '}')
		//	yyerror("missing statement after label");
	}

caseblock_list:						{$$ = NULL;}
|	caseblock_list caseblock  		{ $$ = treeA(CASEBLOCK_LIST_T, 2, $1,$2);}
	;

loop_body:
	'{'								
	stmt_list '}'					{ $$ = treeA(LOOP_BODY_T, 3, $1,$2,$3);}
	;

range_stmt:
	expr_list '=' LRANGE expr		{ $$ = treeA(RANGE_STMT_T, 4, $1,$2,$3,$4);}
|	expr_list LCOLAS LRANGE expr	{ $$ = treeA(RANGE_STMT_T, 4, $1,$2,$3,$4);}
	;

for_header:
	osimple_stmt ';' osimple_stmt ';' osimple_stmt	{ $$ = treeA(FOR_HEADER_T, 5,$1,$2,$3,$4,$5);}
|	osimple_stmt					{ $$ = treeA(FOR_HEADER_T, 1, $1);}
|	range_stmt						{ $$ = treeA(FOR_HEADER_T, 1, $1);}
	;

for_body:
	for_header loop_body			{ $$ = treeA(FOR_BODY_T, 2, $1,$2);}
	;

for_stmt:
	LFOR							
	for_body						{ $$ = treeA(FOR_STMT_T, 2, $1,$2);}
	;

if_header:
	osimple_stmt					{ $$ = treeA(IF_HEADER_T, 1, $1);}
|	osimple_stmt ';' osimple_stmt	{ $$ = treeA(IF_HEADER_T, 3, $1,$2,$3);}
	;

/* IF cond body (ELSE IF cond body)* (ELSE block)? */
if_stmt:
	LIF						
	if_header						
	loop_body				
	elseif_list else		{ if($2->kids[0] == NULL ){yyerror("legal Go, not in VGo");
							}
							else{
								$$ = treeA(IF_STMT_T, 5, $1,$2,$3,$4,$5);}
							}
	;

elseif:
	LELSE LIF 				
	if_header loop_body		{ $$ = treeA(ELSEIF_T, 4, $1,$2,$3,$4);}
	;

elseif_list:				{$$= NULL;}
|	elseif_list elseif		{ $$ = treeA(ELSEIF_LIST_T, 2, $1,$2);}
	;

else:						{$$=NULL;}
|	LELSE compound_stmt		{ $$ = treeA(ELSE_T, 2, $1,$2);}
	;

switch_stmt:
	LSWITCH					
	if_header				
	'{' caseblock_list '}'	{ $$ = treeA(SWITCH_STMT_T, 5, $1,$2,$3,$4,$5);}
	;

select_stmt:
	LSELECT					
	'{' caseblock_list '}'	{ $$ = treeA(SELECT_STMT_T, 4, $1,$2,$3,$4);}
	;

/*
 * expressions
 */
expr:
	uexpr					{ $$ = treeA(EXPR_T, 1, $1);}
|	expr LOROR expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr LANDAND expr		{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr LEQ expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr LNE expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr LLT expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr LLE expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr LGE expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr LGT expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr '+' expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr '-' expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr '|' expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr '^' expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr '*' expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr '/' expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr '%' expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr '&' expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr LANDNOT expr		{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr LLSH expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
|	expr LRSH expr			{ $$ = treeA(EXPR_T, 3, $1,$2,$3);}
	/* not an expression anymore, but left in so we can give a good error */
|	expr LCOMM expr			{ yyerror("legal Go, not in VGo");}
	;

uexpr:
	pexpr					{ $$ = treeA(UEXPR_T, 1, $1);}
|	'*' uexpr				{ $$ = treeA(UEXPR_T, 2, $1,$2);}
|	'&' uexpr				{ $$ = treeA(UEXPR_T, 2, $1,$2);}
|	'+' uexpr				{ $$ = treeA(UEXPR_T, 2, $1,$2);}
|	'-' uexpr				{ $$ = treeA(UEXPR_T, 2, $1,$2);}
|	'!' uexpr				{ $$ = treeA(UEXPR_T, 2, $1,$2);}
|	'~' uexpr
	{
		yyerror("the bitwise complement operator is ^");
	}
|	'^' uexpr				{ $$ = treeA(UEXPR_T, 2, $1,$2);}
|	LCOMM uexpr				{ $$ = treeA(UEXPR_T, 2, $1,$2);}
	;

/*
 * call-like statements that
 * can be preceded by 'defer' and 'go'
 */
pseudocall:
	pexpr '(' ')'								{ $$ = treeA(PSEUDOCALL_T, 3, $1,$2,$3);}
|	pexpr '(' expr_or_type_list ocomma ')'		{ $$ = treeA(PSEUDOCALL_T, 5, $1,$2,$3,$4,$5);}
|	pexpr '(' expr_or_type_list LDDD ocomma ')'	{ $$ = treeA(PSEUDOCALL_T, 6, $1,$2,$3,$4,$5,$6);}
	;

/*
   The following were deleted in order to allow deletion of LBODY hack:
  |	pexpr_no_paren '{' start_complit braced_keyval_list '}'
  |	'(' expr_or_type ')' '{' start_complit braced_keyval_list '}'
	{
		yyerror("cannot parenthesize type in composite literal");
	}
 */
pexpr_no_paren:
	LLITERAL								{ $$ = treeA(PEXPR_NO_PAREN_T, 1, $1);}
|	LLITERALC								{ $$ = treeA(PEXPR_NO_PAREN_T, 1, $1);}
|	LLITERALF								{ $$ = treeA(PEXPR_NO_PAREN_T, 1, $1);}
|	LLITERALN								{ $$ = treeA(PEXPR_NO_PAREN_T, 1, $1);}
|	LLITERALS								{ $$ = treeA(PEXPR_NO_PAREN_T, 1, $1);}
|	name									{ $$ = treeA(PEXPR_NO_PAREN_T, 1, $1);}
|	pexpr '.' sym							{ $$ = treeA(PEXPR_NO_PAREN_T, 3, $1,$2,$3);}
|	pexpr '.' '(' expr_or_type ')'	    	{ $$ = treeA(PEXPR_NO_PAREN_T, 5, $1,$2,$3,$4,$5);}
|	pexpr '.' '(' LTYPE ')'					{ $$ = treeA(PEXPR_NO_PAREN_T, 5, $1,$2,$3,$4,$5);}
|	pexpr '[' expr ']'						{ $$ = treeA(PEXPR_NO_PAREN_T, 4, $1,$2,$3,$4);}
|	pexpr '[' oexpr ':' oexpr ']'			{ $$ = treeA(PEXPR_NO_PAREN_T, 6, $1,$2,$3,$4,$5,$6);}
|	pexpr '[' oexpr ':' oexpr ':' oexpr ']'	{ $$ = treeA(PEXPR_NO_PAREN_T, 8, $1,$2,$3,$4,$5,$6,$7,$8);}
|	pseudocall								{ $$ = treeA(PEXPR_NO_PAREN_T, 1, $1);}
|	convtype '(' expr ocomma ')'			{ $$ = treeA(PEXPR_NO_PAREN_T, 5, $1,$2,$3,$4,$5);}
|	comptype lbrace start_complit braced_keyval_list '}'	{ $$ = treeA(PEXPR_NO_PAREN_T, 5, $1,$2,$3,$4,$5);}
|	fnliteral												{ $$ = treeA(PEXPR_NO_PAREN_T, 1, $1);}

start_complit:
	{
		// composite expression.
		// make node early so we get the right line number.
	}
	;

keyval:
	expr ':' complitexpr						{$$ = treeA(KEYVAL_T, 3,$1, $2,$3);}
	;

bare_complitexpr:
	expr										{$$ = treeA(BARE_COMPLITEXPR_T, 1, $1);}
|	'{' start_complit braced_keyval_list '}'	{$$ = treeA(BARE_COMPLITEXPR_T,4, $1,$2,$3,$4);}
	;

complitexpr:
	expr
|	'{' start_complit braced_keyval_list '}'	{$$ = treeA(COMPLITEXPR_T, 4,$1,$2,$3,$4);}
	;

pexpr:	
	pexpr_no_paren								{$$ = treeA(PEXPR_T, 1,$1);}
|	'(' expr_or_type ')'						{$$ = treeA(PEXPR_T, 3,$1, $2,$3);}
	;

expr_or_type:
	expr										{$$ = treeA(EXPR_OR_TYPE_T, 1,$1);}										
|	non_expr_type	%prec PreferToRightParen	{$$ = treeA(EXPR_OR_TYPE_T, 2,$1, NULL);}
	
name_or_type:
	ntype										{$$ = treeA(NAME_OR_TYPE_T,1,$1);}

lbrace:
	'{'											{$$ = treeA(LBRACE_T,1,$1);}
	;

/*
 * names and types
 *	newname is used before declared
 *	oldname is used after declared
 */
new_name:
	sym						{$$ = treeA(NEW_NAME_T,1,$1);}
	;

dcl_name:
	sym						{$$ = treeA(DCL_NAME_T,1,$1);}
	;

onew_name:  				{$$ = NULL;}
|	new_name				{$$ = treeA(ONEW_NAME_T,1,$1);}
	;

sym:
	LNAME					{ $$ = treeA(SYM_T,1,$1);}	
|	hidden_importsym		{ $$ = treeA(SYM_T,1,$1);}
|	'?'						{ $$ = treeA(SYM_T,1,$1);}	
	;

hidden_importsym:
	'@' LLITERAL '.' LNAME	{ $$ = treeA(HIDDEN_IMPORTSYM_T,4,$1,$2,$3,$4);}
|	'@' LLITERAL '.' '?'	{ $$ = treeA(HIDDEN_IMPORTSYM_T,4,$1,$2,$3,$4);}
	;

name:
	sym	%prec NotParen		{$$ = treeA(NAME_T, 1,$1);}
	;

labelname:
	new_name				{$$= treeA(LABELNAME_T, 1,$1);}
	;

/*
 * to avoid parsing conflicts, type is split into
 *	channel types
 *	function types
 *	parenthesized types
 *	any other type
 * the type system makes additional restrictions,
 * but those are not implemented in the grammar.
 */
dotdotdot:
	LDDD
	{
		yyerror("final argument in variadic function missing type");
	}
|	LDDD ntype 				{$$= treeA(DOTDOTDOT_T, 2,$1,$2);}
	;

ntype:
	recvchantype	 		{$$= treeA(NTYPE_T, 1,$1);}
|	fntype					{$$= treeA(NTYPE_T, 1,$1);}
|	othertype				{$$= treeA(NTYPE_T, 1,$1);}
|	ptrtype					{$$= treeA(NTYPE_T, 1,$1);}
|	dotname					{$$= treeA(NTYPE_T, 1,$1);}
|	'(' ntype ')'			{$$= treeA(NTYPE_T, 3,$1,$2,$3);}
	;

non_expr_type:
	recvchantype			{$$= treeA(NON_EXPR_TYPE_T, 1,$1);}
|	fntype					{$$= treeA(NON_EXPR_TYPE_T, 1,$1);}
|	othertype				{$$= treeA(NON_EXPR_TYPE_T, 1,$1);}
|	'*' non_expr_type		{$$= treeA(NON_EXPR_TYPE_T, 2,$1,$2);}
	;

non_recvchantype:
	fntype					{$$= treeA(NON_RECVCHANTYPE_T, 1,$1);}
|	othertype				{$$= treeA(NON_RECVCHANTYPE_T, 1,$1);}
|	ptrtype					{$$= treeA(NON_RECVCHANTYPE_T, 1,$1);}
|	dotname					{$$= treeA(NON_RECVCHANTYPE_T, 1,$1);}
|	'(' ntype ')'			{$$= treeA(NON_RECVCHANTYPE_T, 3,$1,$2,$3);}
	;

convtype:
	fntype					{$$= treeA(CONVTYPE_T, 1,$1);}
|	othertype				{$$= treeA(CONVTYPE_T, 1,$1);}
	
comptype:
	othertype				{$$= treeA(COMPTYPE_T, 1,$1);}
	;

fnret_type:
	recvchantype			{$$= treeA(FNRET_TYPE_T, 1,$1);}
|	fntype					{$$= treeA(FNRET_TYPE_T, 1,$1);}
|	othertype				{$$= treeA(FNRET_TYPE_T, 1,$1);}
|	ptrtype					{$$= treeA(FNRET_TYPE_T, 1,$1);}
|	dotname					{$$= treeA(FNRET_TYPE_T, 1,$1);}
	;

dotname:
	name					{$$= treeA(DOTNAME_T, 1,$1);}
|	name '.' sym			{$$= treeA(DOTNAME_T, 3,$1,$2,$3);}
	;

othertype:
	'[' oexpr ']' ntype								{$$= treeA(OTHERTYPE_T, 4,$1,$2,$3,$4);}
|	'[' LDDD ']' ntype								{$$= treeA(OTHERTYPE_T, 4,$1,$2,$3,$4);}
|	LCHAN non_recvchantype							{$$= treeA(OTHERTYPE_T, 2,$1,$2);}
|	LCHAN LCOMM ntype								{$$= treeA(OTHERTYPE_T, 4,$1,$2,$3);}
|	LMAP '[' ntype ']' ntype						{$$= treeA(OTHERTYPE_T, 5,$1,$2,$3,$4,$5);}
|	structtype										{$$= treeA(OTHERTYPE_T, 1,$1);}
|	interfacetype									{$$= treeA(OTHERTYPE_T, 1,$1);}
	;

ptrtype:
	'*' ntype										{$$= treeA(PTRTYPE_T, 2,$1,$2);}
	;

recvchantype:
	LCOMM LCHAN ntype								{$$= treeA(RECVCHANTYPE_T, 3,$1,$2,$3);}
	;

structtype:
	LSTRUCT lbrace structdcl_list osemi '}'			{$$= treeA(STRUCTTYPE_T, 5,$1,$2,$3,$4,$5);}
|	LSTRUCT lbrace '}'								{$$= treeA(STRUCTTYPE_T, 3,$1,$2,$3);}
	;

interfacetype:
	LINTERFACE lbrace interfacedcl_list osemi '}'	{$$= treeA(INTERFACETYPE_T, 4,$1,$2,$3,$4);}
|	LINTERFACE lbrace '}'							{$$= treeA(INTERFACETYPE_T, 3,$1,$2,$3);}
	;

/*
 * function stuff
 * all in one place to show how crappy it all is
 */
xfndcl:
	LFUNC fndcl fnbody							{$$= treeA(XFNDCL_T, 3,$1,$2,$3);}
	;

fndcl:
	sym '(' oarg_type_list_ocomma ')' fnres				{$$= treeA(FNDCL_T, 5,$1,$2,$3,$4,$5);}
|	'(' oarg_type_list_ocomma ')' sym '(' oarg_type_list_ocomma ')' fnres {$$= treeA(FNDCL_T, 5,$1,$2,$3,$4,$5);}
	;

fntype:
	LFUNC '(' oarg_type_list_ocomma ')' fnres	{$$= treeA(FNTYPE_T, 4,$1,$2,$3,$4);}
	;

fnbody:					{$$ = NULL;}
|	'{' stmt_list '}' 				{$$ = treeA(FNBODY_T,3,$1,$2,$3);}
	;

fnres:
	%prec NotParen						{$$ = treeA(FNRES_T,1,NULL);}
|	fnret_type							{$$ = treeA(FNRES_T,1,$1);}
|	'(' oarg_type_list_ocomma ')'		{$$ = treeA(FNRES_T,3,$1,$2,$3);}
	;	

fnlitdcl:
	fntype								{$$ = treeA(FNLITDCL_T,1,$1);}
	;

fnliteral:
	fnlitdcl lbrace stmt_list '}'		{$$ = treeA(FNLITERAL_T,4,$1,$2,$3,$4);}
|	fnlitdcl error						{$$ = treeA(FNLITERAL_T,2,$1,$2);}
	;

/*
 * lists of things
 * note that they are left recursive to conserve yacc stack. they need to
 * be reversed to interpret correctly
 */
xdcl_list:	{$$=NULL;}
|	xdcl_list xdcl ';'					{ $$ = treeA(XDCL_LIST_T, 3, $1,$2,$3);}		
	;

vardcl_list:
	vardcl								{$$ = treeA(VARDCL_LIST_T,1,$1);}
|	vardcl_list ';' vardcl				{$$ = treeA(VARDCL_LIST_T,3,$1,$2,$3);}
	;

constdcl_list:
	constdcl1							{$$ = treeA(CONSTDCL_LIST_T,1,$1);}
|	constdcl_list ';' constdcl1			{$$ = treeA(CONSTDCL_LIST_T,3,$1,$2,$3);}
	;

typedcl_list:
	typedcl								{$$ = treeA(TYPEDCL_LIST_T,1,$1);}
|	typedcl_list ';' typedcl			{$$ = treeA(TYPEDCL_LIST_T,3,$1,$2,$3);}
	;

structdcl_list:
	structdcl							{$$ = treeA(STRUCTDCL_LIST_T,1,$1);}
|	structdcl_list ';' structdcl		{$$ = treeA(STRUCTDCL_LIST_T,3,$1,$2,$3);}
	;

interfacedcl_list:
	interfacedcl						{$$ = treeA(INTERFACEDCL_LIST_T,1,$1);}
|	interfacedcl_list ';' interfacedcl	{$$ = treeA(INTERFACEDCL_LIST_T,3,$1,$2,$3);}
	;

structdcl:
	new_name_list ntype oliteral		{$$ = treeA(STRUCTDCL_T,3,$1,$2,$3);}
|	embed oliteral						{$$ = treeA(STRUCTDCL_T,2,$1,$2);}
|	'(' embed ')' oliteral				{$$ = treeA(STRUCTDCL_T,4,$1,$2,$3,$4);}
|	'*' embed oliteral					{$$ = treeA(STRUCTDCL_T,3,$1,$2,$3);}
|	'(' '*' embed ')' oliteral			{$$ = treeA(STRUCTDCL_T,5,$1,$2,$3,$4,$5);}
|	'*' '(' embed ')' oliteral			{$$ = treeA(STRUCTDCL_T,5,$1,$2,$3,$4,$5);}
	;

packname:
	LNAME								{$$ = treeA(PACKNAME_T,1,$1);}	
|	LNAME '.' sym						{$$ = treeA(PACKNAME_T,1,$1,$2,$3);}	
	;

embed:
	packname							{$$ = treeA(EMBED_T,1,$1);}	
	;

interfacedcl:
	new_name indcl						{$$ = treeA(INTERFACEDCL_T,1,$1);}	
|	packname							{$$ = treeA(INTERFACEDCL_T,1,$1);}
|	'(' packname ')'					{$$ = treeA(INTERFACEDCL_T,3,$1,$2,$3);}
	;

indcl:
	'(' oarg_type_list_ocomma ')' fnres		{$$ = treeA(INDCL_T,4,$1,$2,$3,$4);}
	;

/*
 * function arguments.
 */
arg_type:
	name_or_type						{$$ = treeA(ARG_TYPE_T,1,$1);}	
|	sym name_or_type					{$$ = treeA(ARG_TYPE_T,2,$1,$2);}
|	sym dotdotdot						{$$ = treeA(ARG_TYPE_T,2,$1,$2);}
|	dotdotdot							{$$ = treeA(ARG_TYPE_T,1,$1);}
	;

arg_type_list:		
	arg_type							{$$ = treeA(ARG_TYPE_LIST_T,1,$1);}
|	arg_type_list ',' arg_type			{$$ = treeA(ARG_TYPE_LIST_T,3,$1,$2,$3);}
	;

oarg_type_list_ocomma:	{$$ = NULL;}
|	arg_type_list ocomma				{$$ = treeA(OARG_TYPE_LIST_OCOMMA_T,2,$1,$2);}
	;

/*
 * statement
 */
stmt:			{$$ = NULL;}
|	compound_stmt						{ $$ = treeA(STMT_T, 1, $1);}
|	common_dcl							{ $$ = treeA(STMT_T, 1, $1);}
|	non_dcl_stmt						{ $$ = treeA(STMT_T, 1, $1);}
|	error								{ $$ = treeA(STMT_T, 1, $1);}
	;

non_dcl_stmt:
	simple_stmt							{ $$ = treeA(NON_DCL_STMT_T, 1, $1);}
|	for_stmt							{ $$ = treeA(NON_DCL_STMT_T, 1, $1);}
|	switch_stmt							{ $$ = treeA(NON_DCL_STMT_T, 1, $1);}
|	select_stmt							{ $$ = treeA(NON_DCL_STMT_T, 1, $1);}
|	if_stmt								{ $$ = treeA(NON_DCL_STMT_T, 1, $1);}
|	labelname ':'						
	stmt								{ $$ = treeA(NON_DCL_STMT_T, 2, $1,$2,$3);}
|	LFALL								{ $$ = treeA(NON_DCL_STMT_T, 1, $1);}
|	LBREAK onew_name					{ $$ = treeA(NON_DCL_STMT_T, 2, $1,$2);}
|	LCONTINUE onew_name					{ $$ = treeA(NON_DCL_STMT_T, 2, $1,$2);}
|	LGO pseudocall						{ $$ = treeA(NON_DCL_STMT_T, 2, $1,$2);}
|	LDEFER pseudocall					{ $$ = treeA(NON_DCL_STMT_T, 2, $1,$2);}
|	LGOTO new_name						{ $$ = treeA(NON_DCL_STMT_T, 2, $1,$2);}
|	LRETURN oexpr_list					{ $$ = treeA(NON_DCL_STMT_T, 2, $1,$2);}
	;

stmt_list:
	stmt								{ $$ = treeA(STMT_LIST_T, 1, $1);}
|	stmt_list ';' stmt					{ $$ = treeA(STMT_LIST_T, 3, $1,$2,$3);}
	;

new_name_list:
	new_name							{ $$ = treeA(NEW_NAME_LIST_T, 1, $1);}	
|	new_name_list ',' new_name			{ $$ = treeA(NEW_NAME_LIST_T, 3, $1,$2,$3);}
	;

dcl_name_list:
	dcl_name							{ $$ = treeA(DCL_NAME_LIST_T, 1, $1);}
|	dcl_name_list ',' dcl_name			{ $$ = treeA(DCL_NAME_LIST_T, 3, $1,$2,$3);}
	;

expr_list:
	expr								{$$= treeA(EXPR_LIST_T,1,$1);}	
|	expr_list ',' expr					{$$= treeA(EXPR_LIST_T,1,$1,$2,$3);}	
	;

expr_or_type_list:
	expr_or_type						{ $$ = treeA(EXPR_OR_TYPE_LIST_T, 1, $1);}	
|	expr_or_type_list ',' expr_or_type	{ $$ = treeA(EXPR_OR_TYPE_LIST_T, 3, $1,$2,$3);}
	;

/*
 * list of combo of keyval and val
 */
keyval_list:
	keyval								{$$ = treeA(KEYVAL_LIST_T, 1, $1);}
|	bare_complitexpr					{$$ = treeA(KEYVAL_LIST_T, 1, $1);}
|	keyval_list ',' keyval				{$$ = treeA(KEYVAL_LIST_T, 3, $1,$2,$3);}
|	keyval_list ',' bare_complitexpr	{$$ = treeA(KEYVAL_LIST_T, 3, $1,$2,$3);}
	;

braced_keyval_list: 					{$$ = NULL;}
|	keyval_list ocomma					{$$ = treeA(BRACED_KEYVAL_LIST_T, 2, $1,$2);}
	;

/*
 * optional things
 */
osemi:									{$$ = NULL;}
|	';'									{$$= treeA(OSEMI_T,1,$1);}
	;

ocomma: 								{$$=NULL;}
|	','									{$$= treeA(OCOMMA_T,1,$1);}	
	;

oexpr: 									{$$ = NULL;}
|	expr								{$$= treeA(OEXPR_T,1,$1);}
	;

oexpr_list:								{$$=NULL;}
|	expr_list							{$$= treeA(OEXPR_LIST_T,1,$1);}
	;

osimple_stmt:							{$$ = NULL;}
|	simple_stmt							{$$= treeA(OSIMPLE_STMT_T,1,$1);}
	;

oliteral:								{$$ = NULL;}
|	LLITERAL							{$$= treeA(OLITERAL_T,1,$1);}
|	LLITERALC							{$$= treeA(OLITERAL_T,1,$1);}		
|	LLITERALF							{$$= treeA(OLITERAL_T,1,$1);}
|	LLITERALN							{$$= treeA(OLITERAL_T,1,$1);}
|	LLITERALS							{$$= treeA(OLITERAL_T,1,$1);}
	;

%%