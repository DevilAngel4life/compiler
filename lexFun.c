#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vgo.tab.h"
#include "lexe.h"
#include "link.h"
#include "tree.h"

/** 
*   This function takes int number, a char pointer and number on which line
*   it occured it creates memory allocation for the global token and.
*   this funciton also sets the last token seen and the tree node to yylval
*  
*   @param int category   
*   @param char * txt
*   @param int lineNumber 
*/
void strTok(int cat, char *txt, int lin)
{
    lasttoken = cat;
    token_at = malloc(sizeof(token));
    token_at->category = cat;
    token_at->text = txt;
    token_at->lineno = lin;
    token_at->filename = currFileName;

    // printf("                                next token is -->%s\n", txt);

    if (cat == LLITERALN)
    {
        int num;
        if ((num = iVal(txt)) != 0)
        {
            token_at->ival = num;
        }
    }
    else
    {
        token_at->ival = 0;
    }

    if (cat == LLITERALF)
    {
        double num;
        if ((num = dVal(txt)) != 0)
        {
            token_at->dval = num;
        }
    }
    else
    {
        token_at->dval = 0;
    }

    if (cat == LLITERALS)
    {
        char *temp = NULL;
        temp = SVal(txt);
        token_at->sval = temp;
    }
    else
    {
        token_at->sval = NULL;
    }

    push(token_at);
    yylval.t = getLeaf(token_at);
}

/** 
*   This function takes a charater pointer checks how many double quotes 
*   it has and removes them than returns a char *ptr

*   @param char * txt
*   @return char *
*/
char *SVal(char *txt)
{
    int len;
    int subs = 0;
    len = strlen(txt);
    for (int i = 0; i < len; i++)
    {
        if ((txt[i] == '\\') || (txt[i] == '"'))
        {
            subs++;
        }
    }
    char *a;
    a = calloc(1, sizeof(char) * (len - subs + 1));
    int ii = 0;
    for (int i = 0; i < len; i++)
    {

        if (txt[i] == '"')
        {
            continue;
        }
        if (txt[i] == '\\')
        {

            switch (txt[i + 1])
            {
            case 't':
                a[ii] = '\t';
                i++;
                break;
            case 'n':
                a[ii] = '\n';
                i++;
                break;
            }
        }
        else
        {
            a[ii] = txt[i];
        }
        ii++;
    }
    return a;
}

/** 
*   Returns the int value from text

*   @param char * txt
*   @return int
*/
int iVal(char *txt)
{
    int val;
    val = atoi(txt);
    return val;
}

/** 
*   Returns the double value from the text

*   @param char * txt
*   @return double
*/
double dVal(char *txt)
{
    double val;
    val = atof(txt);
    return val;
}

/* return true for tokens that require a newline to insert a semi-colon */
int isender(int i)
{
    switch (i)
    {
    case LNAME:
    case LLITERALS:
    case LLITERALF:
    case LLITERALN:
    case LLITERALC:
    case ')':
    case '}':
    case ']':
        return 1;
    }
    return 0;
}

/** 
*   Function to create a ptr to a ';' to be used
*
*   @return char *ptr
*/
char *CreateSemi()
{
    char *t;
    t = ";";
    return t;
}