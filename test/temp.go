package main
// A test of the for loop syntax

func random() int {
	return 1
}

func main() {


var test8 int
		// No second or third statement
	for test8 = 0;; {
		if {
		   return
		}
		test8 += 1
	}
}
	