cc=gcc
ccopts=-c -g -std=gnu99
CFLAGS=-Wall
ver=2
lex=flex
lexgen=lex.yy.c
pj=vgo
lexrule=vgo.l
man=main

#this is for the lexer only
$(pj):  prodRules.o $(man).o tree.o list.o vgo.tab.o lexFun.o lex.yy.o 
	$(cc) prodRules.o $(man).o  tree.o lex.yy.o  list.o lexFun.o vgo.tab.o -o $(pj)

$(man).o: $(man).c lexe.h 
	$(cc) $(ccopts) $(CFLAGS) $(man).c

list.o: list.c 
	$(cc) $(ccopts) $(CFLAGS) list.c

lexFun.o: lexFun.c vgo.tab.h
	$(cc) $(ccopts) $(CFLAGS) lexFun.c

lex.yy.o : $(lexgen) lexe.h
	$(cc) -c -g $(CFLAGS) $(lexgen)
	
$(lexgen): $(lexrule) 
	$(lex) $(lexrule)

vgo.tab.o: vgo.tab.c vgo.tab.h
	$(CC)  $(ccopts) $(CFLAGS) vgo.tab.c

vgo.tab.c vgo.tab.h: vgo.y
	bison -d vgo.y

prodRules.o: prodRules.c prodRules.h	
	$(cc) $(ccopts) $(CFLAGS) prodRules.c

tree.o: tree.c tree.h 
	$(cc) $(ccopts) $(CFLAGS) tree.c
#this cleans only files the lexer produced
clean:
	rm $(lexgen) vgo.tab.c vgo.tab.h $(pj) *.o 

shipit: 
	zip -r hw$(ver).zip --exclude=.vscode/* --exclude=*.git* --exclude=*.DS_Store* --exclude=*.go --exclude=*.test/* --exclude=hello --exclude=comm.gO . 

transfer: clean shipit
	scp  ./hw$(ver).zip mend9654@cs-445.cs.uidaho.edu:/home/mend9654/schoolhw/cs445/temp/