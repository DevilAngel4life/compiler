#ifndef LINK_FILE
#define LINK_FILE
// This file is for the prototypes of the link.c
typedef struct tokenlist
{
    struct token *t;
    struct tokenlist *next;
} tokenList;
tokenList *currentNode;
tokenList *headNode;

int flagHead;
void init();
void setHead();
void push(struct token *data); // void push(listNode *head, int val);
void print_list(tokenList *head);
#endif