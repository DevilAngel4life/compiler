
%{
/*
        Lexer HW1 Commpilers 
        Editor Jorge L Mendoza
        Comme Regex Source Chris Dodd https://stackoverflow.com/questions/13569827/complete-comments-regex-for-lex         
        Flop Regex Source https://www.regular-expressions.info/floatingpoint.html 
 */
 #include <stdio.h>
 #include <stdlib.h>
 #include <math.h>
 #include <string.h>
 #include "vgo.tab.h"
 #include "lexe.h"
 #include "link.h"
 
//  extern YYSTYPE yylval;

%}
%option noinput
%option nounput
%option yylineno
  
SCOMME      \/\/.*
COMME	    "/*"([^*]|\*+[^*/])*\*+"/"                    
STRING      \"(\\.|[^\\"])*\"
CHARLIT     '([a-zA-Z]|\\[nt\\''])'
BADRUNE      '([a-zA-Z]{2,}|\\[nt\\''a-zA-Z0-9]{2,})'        
FLOP        [-+]?[0-9]*\.[0-9]+([eE][-+]?[0-9]+)?         
LNAME       [a-zA-Z_][a-zA-Z0-9_]*
IMAGLIT     [-+]?([0-9])([a-zA-Z]*)[0-9a-fA-F_.]*i|[-+]?[0-9]*\.[0-9]+([eE][-+]?[0-9]+)?i 
NUMLIT      [-+]?([0-9])([a-zA-Z]*)[0-9a-fA-F_.]*



LNE         !=
LLE         \<(?:\=) 
LLT         '<'
LGE         \>(?:\=)
LGT         '>'
LEQ         \=(?:\=)
LANDAND     &{2}
LOROR       \|\|
LASOP       \+=|\-=
LINC        \+\+ 
LDEC        \-\- 


%%
    /*

     */
"\n"					{  if (isender(lasttoken)){
                            // char * temp; 
                            // temp = ";";
                            strTok(';',(char *)strdup(CreateSemi()),yylineno);
                            return ';';
                            } ;
                         }
[\t\f\v\r ]+	{ /* Ignore whitespace. */ }
{COMME}     { 
            errorW(yytext,yylineno ); 
            exit(2);
            }
break |
default |
interface |
select |
case |
defer |
go | 
chan | 
goto | 
switch |
fallthrough |
range |
continue {
    /* The action to be taken when we encounter a keyword */
                    errorW(yytext,yylineno);
                    exit(2);
    }

{BADRUNE}   {printf("Error %s line %d in file %s is an not support in VGo\n",yytext,yylineno,currFileName );
            exit(1);}            
{SCOMME}    {strTok(LCOMM,(char *) strdup(yytext),yylineno);
            }

"const"     { strTok(LCONST,(char *)strdup(yytext),yylineno);
            return LCONST;}
"else"      { strTok(LELSE,(char *)strdup(yytext),yylineno);
            return LELSE;}
"for"       { strTok(LFOR,(char *)strdup(yytext),yylineno);
             return LFOR;}
"func"      { strTok(LFUNC,(char *)strdup(yytext),yylineno);
            return LFUNC; }
"if"        { strTok(LIF,(char *)strdup(yytext),yylineno);
            return LIF;}
"import"    {strTok(LIMPORT, (char *)strdup(yytext),yylineno);
            return LIMPORT;}
"map"       { strTok(LMAP,(char *)strdup(yytext),yylineno);
            return LMAP;}
"package"   { strTok(LPACKAGE,(char *)strdup(yytext),yylineno);
            return LPACKAGE;}
"struct"    { strTok(LSTRUCT,(char *)strdup(yytext),yylineno);
            return LSTRUCT;}
"return"    { strTok(LRETURN,(char *)strdup(yytext),yylineno);
            return LRETURN;}
"type"      { strTok(LTYPE,(char *)strdup(yytext),yylineno);
            return LTYPE;}
"var"       { strTok(LVAR,(char *)strdup(yytext),yylineno);
            return LVAR;}

"bool"|"string"|"int"|"float64"      { strTok(LNAME,(char *)strdup(yytext),yylineno);
            return LNAME;}

"&"|"&="|"|"|"|="|"^"|"*="|"^="|"<-"|"<<"|"/="|"<<="|":="|">>"|"%="|">>="|":"|"&^"|"&^="  { 
                errorW(yytext,yylineno );
                }

{STRING}    {strTok(LLITERALS,(char *)strdup(yytext),yylineno);
            return LLITERALS;}
{CHARLIT}   {strTok(LLITERALC,(char *)strdup(yytext),yylineno);
            return LLITERALC;}
{FLOP}     {strTok(LLITERALF,(char *)strdup(yytext),yylineno);
            return LLITERALF;}
{NUMLIT}    {strTok(LLITERALN,(char *)strdup(yytext),yylineno);
            return LLITERALN;}
{IMAGLIT}      {printf("Error %s line %d in file %s is an not support in VGo\n",yytext,yylineno, currFileName );
            exit(1);}


"("          {strTok('(',(char *)strdup(yytext),yylineno);
            return '(';}
")"           {strTok(')',(char *)strdup(yytext),yylineno);
            return ')';}
"["          {strTok('[',(char *)strdup(yytext),yylineno);
             return '[';}
"]"          {strTok(']',(char *)strdup(yytext),yylineno);
             return ']';}
"."          {strTok('.',(char *)strdup(yytext),yylineno);
            return '.';}
"!="        {strTok(LNE,(char *)strdup(yytext),yylineno);
            return LNE;}
"!"          {strTok('!',(char *)strdup(yytext),yylineno);
            return '!';}
"*"          {strTok('*',(char *)strdup(yytext),yylineno);
            return '*';}
"/"          {strTok('/',(char *)strdup(yytext),yylineno);
            return '/';}
"%"          {strTok('%',(char *)strdup(yytext),yylineno);
            return '%';}
"+"          {strTok('+',(char *)strdup(yytext),yylineno);
            return '+';}
"-"          {strTok ('-',(char *)strdup(yytext),yylineno);
            return '-';}
{LLE}       {strTok(LLE,(char *)strdup(yytext),yylineno);
            return LLE;}
"<"         {strTok(LLT,(char *)strdup(yytext),yylineno);
            return LLT;}
{LGE}       {strTok(LGE,(char *)strdup(yytext),yylineno);
            return LGE;}
">"         {strTok(LLT,(char *)strdup(yytext),yylineno);
            return LGT;}
{LEQ}       {strTok(LEQ,(char *)strdup(yytext),yylineno);
            return LEQ;} 
{LANDAND}   {strTok(LANDAND,(char *)strdup(yytext),yylineno);
            return LANDAND;} 
{LOROR}     {strTok(LOROR,(char *)strdup(yytext),yylineno);
            return LOROR;}
{LASOP}     {strTok(LASOP,(char *)strdup(yytext),yylineno);
            return LASOP;} 
{LINC}      {strTok(LINC,(char *)strdup(yytext),yylineno);
            return LINC;}
{LDEC}      {strTok(LDEC,(char *)strdup(yytext),yylineno);
            return LDEC;}
";"          {strTok(';',(char *)strdup(yytext),yylineno);
            return ';';}
","          {strTok(',',(char *)strdup(yytext),yylineno);
            return ',';} 
"{"          {strTok('{',(char *)strdup(yytext),yylineno);
            return '{';}
"}"          {strTok('}',(char *)strdup(yytext),yylineno);
            return '}';}   
"="          {strTok('=',(char *)strdup(yytext),yylineno);
            return '=';}

{LNAME}      { if(strlen(yytext)<12){strTok(LNAME,(char *) strdup(yytext), yylineno);
            return LNAME;
            }else
            {      
                char * tempStr = malloc(sizeof(char)*13);
                strncpy(tempStr,yytext, 12);
                strTok(LNAME, tempStr, yylineno);
                return LNAME;
            };
            }     

.          { printf("Error: Unrecognized character: %s in line %d file name %s\n", yytext,yylineno,currFileName);
                exit(1);
         }

<<EOF>>     { if (isender(lasttoken)){
                    strTok(';',(char *)strdup(CreateSemi()),yylineno);
                    return ';';
                }else{
                    return -1; 
                }
            };
     
%%

void errorW(char *ty, int lin){
    // i need to print it with file name and the line number
    printf("Go %s is not in VGo line %d in file %s\n", ty, lin,currFileName);
    exit(1);
}

int yywrap()
{
    yylineno = 1; // reset the number of line from the new file
    return 1;
}

