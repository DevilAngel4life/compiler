#ifndef LEXE_H
#define LEXE_H

// list of variable in vgo.l

// list of structs
// int yyparse();
// int yylex();

typedef struct token
{
    int category;   /* the integer code returned by yylex */
    char *text;     /* the actual string (lexeme) matched */
    int lineno;     /* the line number on which the token occurs */
    char *filename; /* the source file in which the token occurs */
    int ival;       /* for integer constants, store binary value here */
    double dval;    /* for real constants, store binary value here */
    char *sval;     /* for string constants, malloc space, de-escape, store */

} token;

// list of pointers
token *token_at;    // current pointer for storing
char *currFileName; // name of the current parsing file
int lasttoken;      // The last token that passed through the lexer
int yylineno;
char *yytext;
int flag;

// list of functions in vgo.l
int isender(int lasttoken);               // semicolon insertion.
void strTok(int cat, char *txt, int lin); // token store in list
void errorW(char *ty, int lin);           // prints errors in the parsing s
int iVal(char *tx);                       // iVal checking returns the int number if true
double dVal(char *tx);                    // dval checking retunrs a double if true
char *SVal(char *tx);                     // returns a string with the " " in the string
char *CreateSemi();

#endif /*End of Sym*/
