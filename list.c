#include <stdio.h>
#include <stdlib.h>

#include "link.h"
#include "lexe.h"

/*
* This file is in charge all of the link list only
* along with printing of the link list
*/
void init()
{
    // initizilates the first node for the link list
    currentNode = malloc(sizeof(tokenList));
    headNode = NULL;
}

void setHead(tokenList *current)
{
    headNode = currentNode;
}

void push(token *data)
{
    tokenList *current;
    current = malloc(sizeof(tokenList));
    current->t = data;
    current->next = NULL;
    currentNode->next = current;
    currentNode = current;

    if (!headNode)
    {
        //sets the head to the first node
        setHead(current);
    }
    else if (flagHead != 1)
    { // points the head to the next node
        flagHead = 1;
        headNode->next = current;
    }
}

void print_list(tokenList *head)
{
    tokenList *current = head;

    printf("\n%-15s%-15s%-15s%-15s%-10s\n", "Category", "Text", "Lineno", "Filename", "Ival/Sval");
    printf("---------------------------------------------------------------------------\n");

    while (current != NULL)
    {

        printf("%-15d%-15s%-15d%-15s",
               current->t->category, current->t->text,
               current->t->lineno, current->t->filename);

        if (current->t->ival != 0)
        {
            printf("%-10d", current->t->ival);
        };
        if (current->t->dval)
        {
            printf("%-10f", current->t->dval);
        }
        if (current->t->sval != NULL)
        {
            printf("%-10s", current->t->sval);
        }

        printf("\n");
        current = current->next;
    }
}
