#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lexe.h"
#include "link.h"
#include "tree.h"

FILE *yyin;
char __File__Buffer[1000];
int yyparse();

int main(int argc, char **argv)
{
    const char *extention = ".go";

    if (argc > 1)
    { //init link list
        init();
        for (int i = 1; i < argc; i++)
        {
            currentT = i;
            char *tempString = (char *)malloc(100);

            yyin = fopen(argv[i], "r");

            if (!yyin)
            { // error out
                printf("ERROR: Opening the file %s \n", argv[i]);
                return 2;
            }

            if (strcasestr(argv[i], extention))
            {
                tempString = argv[i];
            }
            else
            {
                // NEEDS TO CREATE BUFF
                //add it to an array and attach at the end .vO
                tempString = strcpy(tempString, argv[i]);
                tempString = (char *)realloc(tempString, sizeof(char) * (4 + strlen(argv[i])));
                strcat(tempString, ".go");
            }
            currFileName = tempString;

            yyparse();

            if (nextTree[i] == NULL)
            {
                printf("Error: global_tree is empty.\n");
                exit(1);
            }
            else
            {
                printTree(nextTree[i], 1);
            }
        };
    }
    else
    {
        yyin = stdin;
        currFileName = "stdin";
    }

    // print_list(headNode); // link list
}
